import Component from '@ember/component';

export default Component.extend({
	actions:{
		agregarProduct(producto){
			let food = this.get('food')
			
			food.set('product', producto)
			this.set('productoSelected', producto)
		}
	}
});
