import Controller from '@ember/controller';

export default Controller.extend({
	actions:{
		updateProduct(product){
			product.save().then(()=>{
				alert("Producto actualizado");
			}).catch((e)=>{
				alert('Upsss hubo un error al actualizar el producto');
			})
		},
		deleteProduct(product){
			product.destroyRecord().then(()=>{
				alert('El producto ha sido eliminado')
			}).catch((e)=>{
				alert('Hubo un error al eliminar')
			})
		},
		createProductRecord(){
			this.store.createRecord('product',{
				name: 'nuevo',
				calories: 0
			})
		}
	}
});
