import Controller from '@ember/controller';
import {inject as service} from '@ember/service';

export default Controller.extend({
  calculator: service(),
  actions: {
    agregar() {
      this.store.createRecord('food', {
        meal: this.get('model.meal'),
      })


    },
    guardar(meal) {
      meal.get('foods').then((foods) => {

        foods.forEach(function(food) {

          food.save();
        })

        meal.save().then(() => {
          let calories = this.get('model.meal.totalCalories');
          console.log(calories)
          this.get('calculator').add(calories)
          alert('Saved!')
        })
      })

    }
  }
});
