import Controller from '@ember/controller';

export default Controller.extend({
	actions: {
		updateMeal(meal){
			meal.save().then(()=>{
				alert('El meal ha sido actualizada.');
			});
		},
		deleteMeal(meal){
			meal.destroyRecord().then(()=>{
				alert("Se ha borrado el meal.");
			});
		},
		addMeal(){
			this.store.createRecord('meal')
		}

	}
});
