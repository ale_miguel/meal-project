import DS from 'ember-data';
import {computed} from '@ember/object';


export default DS.Model.extend({
  name: DS.attr('string'),
  createdAt: DS.attr('date'),
  foods: DS.hasMany('food'),

  description: computed('name', function(){
  	return `${this.get('name')} is a delicious meal, fill it with everything you ate!`;
  }),

  foodCalories: computed('foods.@each.calories', function(){
  	return this.foods.mapBy('calories'); //[100, 24, 32]
  }),

  totalCalories: computed.sum('foodCalories')
});
